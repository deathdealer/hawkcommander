﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLink.Protocol
{
    public struct ControlChar
    {
        ControlChars Char { get; }
        public ControlChar(ControlChars chr)
        {
            Char = chr;
        }

        public static implicit operator byte(ControlChar cc)
        {
            return (byte)cc.Char;
        }
    }

    public enum ControlChars : byte
    {
        /// <summary>
        /// Null
        /// </summary>
        NUL = 0,

        /// <summary>
        /// Start of Heading
        /// </summary>
        [ControlCharType(ControlCharType.CommunicationControl)]
        SOH = 1,

        /// <summary>
        /// Start of Text
        /// </summary>
        [ControlCharType(ControlCharType.CommunicationControl)]
        STX = 2,

        /// <summary>
        /// End of Text
        /// </summary>
        [ControlCharType(ControlCharType.CommunicationControl)]
        ETX = 3,

        /// <summary>
        /// End of Transmission
        /// </summary>
        [ControlCharType(ControlCharType.CommunicationControl)]
        EOT = 4,

        /// <summary>
        /// Enquiry
        /// </summary>
        [ControlCharType(ControlCharType.CommunicationControl)]
        ENQ = 5,

        /// <summary>
        /// Acknowledge
        /// </summary>
        [ControlCharType(ControlCharType.CommunicationControl)]
        ACK = 6,

        /// <summary>
        /// Bell
        /// </summary>
        BEL = 7,

        /// <summary>
        /// Backspace
        /// </summary>
        [ControlCharType(ControlCharType.FormatEffector)]
        BS = 8,

        /// <summary>
        /// Horizontal Tabulation
        /// </summary>
        [ControlCharType(ControlCharType.FormatEffector)]
        TAB = 9,

        /// <summary>
        /// Line Feed
        /// </summary>
        [ControlCharType(ControlCharType.FormatEffector)]
        LF = 10,

        /// <summary>
        /// Vertical Tabulation
        /// </summary>
        [ControlCharType(ControlCharType.FormatEffector)]
        VT = 11,

        /// <summary>
        /// Form Feed
        /// </summary>
        [ControlCharType(ControlCharType.FormatEffector)]
        FF = 12,

        /// <summary>
        /// Carriage Return
        /// </summary>
        [ControlCharType(ControlCharType.FormatEffector)]
        CR = 13,

        /// <summary>
        /// Shift Out
        /// </summary>
        SO = 14,

        /// <summary>
        /// Shift In
        /// </summary>
        SI = 15,

        /// <summary>
        /// Data Link Escape
        /// </summary>
        [ControlCharType(ControlCharType.CommunicationControl)]
        DLE = 16,

        /// <summary>
        /// <para>Device Control 1</para>
        /// <para>XON</para>
        /// </summary>
        DC1 = 17,

        /// <summary>
        /// <para>Device Control 2</para>
        /// </summary>
        DC2 = 18,

        /// <summary>
        /// <para>Device Control 3</para>
        /// <para>XOFF</para>
        /// </summary>
        DC3 = 19,

        /// <summary>
        /// <para>Device Control 4</para>
        /// </summary>
        DC4 = 20,

        /// <summary>
        /// Negative Acknowledge
        /// </summary>
        [ControlCharType(ControlCharType.CommunicationControl)]
        NAK = 21,

        /// <summary>
        /// Synchronous Idle
        /// </summary>
        [ControlCharType(ControlCharType.CommunicationControl)]
        SYN = 22,

        /// <summary>
        /// End of Transmission Block
        /// </summary>
        [ControlCharType(ControlCharType.CommunicationControl)]
        ETB = 23,

        /// <summary>
        /// Cancel
        /// </summary>
        CAN = 24,

        /// <summary>
        /// End of Medium
        /// </summary>
        EM = 25,

        /// <summary>
        /// Substitute
        /// </summary>
        SUB = 26,

        /// <summary>
        /// Escape
        /// </summary>
        ESC = 27,

        /// <summary>
        /// File Separator
        /// </summary>
        [ControlCharType(ControlCharType.InformationSeparator)]
        FS = 28,

        /// <summary>
        /// Group  Separator
        /// </summary>
        [ControlCharType(ControlCharType.InformationSeparator)]
        GS = 29,

        /// <summary>
        /// Record Separator
        /// </summary>
        [ControlCharType(ControlCharType.InformationSeparator)]
        RS = 30,

        /// <summary>
        /// Unit Separator
        /// </summary>
        [ControlCharType(ControlCharType.InformationSeparator)]
        US = 31
    }
    public class ControlCharTypeAttribute : Attribute
    {
        public ControlCharType ControlCharType { get; }
        public ControlCharTypeAttribute(ControlCharType type)
        {
            ControlCharType = type;
        }
    }
    public enum ControlCharType
    {
        CommunicationControl,
        FormatEffector,
        InformationSeparator
    }
}
