﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLink.Protocol.Frames
{
    public abstract class IntroduceFrameBase : Frame
    {
        private Preamble _preamble = new LongPreamble();
        public override Preamble Preamble
        {
            get { return _preamble; }
        }

        private Header _header = new Header(new ControlChar(ControlChars.ENQ), (byte)'0');
        public override Header Header
        {
            get { return _header; }
        }
        private ByteArray _message = null;
        public override ByteArray Message
        {
            get
            {
                if (_message == null)
                {
                    _message = new ByteArray(EncodeValue(ProtocolId));
                }
                return _message;
            }
        }

        private CheckSum _checkSum = null;
        public override CheckSum CheckSum
        {
            get
            {
                if (_checkSum == null)
                    _checkSum = new Crc32Checksum(new IDataBlock[] { Header, Message });
                return _checkSum;
            }
        }


        protected abstract Int64 ProtocolId { get; }
    }

}
