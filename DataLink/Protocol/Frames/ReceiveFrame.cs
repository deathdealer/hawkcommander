﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLink.Protocol.Frames
{
    public class ReceiveFrame : Frame
    {
        public override Preamble Preamble
        {
            get { return null; }
        }

        public override Header Header
        {
            get
            {
                return _header;
            }
        }
        private DataBlockCreator<Header> _header = new DataBlockCreator<Header>((a) => new Header(a.First()));
        public void AppendHeader(params byte[] b)
        {
            _header.DataBuilder.AddRange(b);
        }

        public override ByteArray Message
        {
            get
            {
                return _message;
            }
        }
        private DataBlockCreator<ByteArray> _message = new DataBlockCreator<ByteArray>((a) => new ByteArray(a.ToArray()));
        public void AppendMessage(params byte[] b)
        {
            _message.DataBuilder.AddRange(b);
        }

        public override CheckSum CheckSum
        {
            get
            {
                return _checksum;
            }
        }
        private DataBlockCreator<CheckSum> _checksum = new DataBlockCreator<CheckSum>((a) =>
        {
            var crcTypes = System.Reflection.Assembly.GetExecutingAssembly().GetTypes().Where(t => typeof(CheckSum).IsAssignableFrom(t));
            foreach (var crc in crcTypes)
            {
                var engine = (CheckSum)Activator.CreateInstance(crc, a.Skip(1));
                if (engine.Identifier.Data.First() == a.First())
                    return engine;
            }
            return null;
        });
        public void AppendCheckSum(params byte[] b)
        {
            _checksum.DataBuilder.AddRange(b);
        }


        private class DataBlockCreator<T> where T : IDataBlock
        {
            public List<byte> DataBuilder { get; }
            public static implicit operator T(DataBlockCreator<T> dbc)
            {
                return (T)dbc.ConstructorBody.Invoke(dbc.DataBuilder);
            }
            public delegate IDataBlock ConstructorBodyDelegate(IEnumerable<byte> data);
            private ConstructorBodyDelegate ConstructorBody { get; }
            public DataBlockCreator(ConstructorBodyDelegate constructorBody)
            {
                ConstructorBody = constructorBody;
            }
        }
    }
}
