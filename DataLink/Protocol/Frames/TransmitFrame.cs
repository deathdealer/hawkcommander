﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLink.Protocol.Frames
{
    public class TransmitFrame : Frame
    {
        public TransmitFrame(Header header, ByteArray message)
        {
            _header = header;
            _message = message;
        }

        private Header _header;
        public override Header Header
        {
            get { return _header; }
        }

        private ByteArray _message;
        public override ByteArray Message
        {
            get { return _message; }
        }

        private Preamble _preamble = new ShortPreamble();
        public override Preamble Preamble
        {
            get { return _preamble; }
        }
    }
}
