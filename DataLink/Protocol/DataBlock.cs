﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLink.Protocol
{
    /// <summary>
    /// Provides access to data contained by data block.
    /// </summary>
    public interface IDataBlock : IEquatable<IDataBlock>
    {
        /// <summary>
        /// The data itself.
        /// </summary>
        IEnumerable<byte> Data { get; }
    }


    /// <summary>
    /// Base class for all data blocks.
    /// </summary>
    public abstract class DataBlock : IDataBlock
    {
        public abstract IEnumerable<byte> Data { get; }

        public virtual bool Equals(IDataBlock other)
        {
            return DataBlockEqualityComparer.AreEqual(this, other);
        }

        /// <summary>
        /// Encodes passed value in byte array that can be stored in data block.
        /// </summary>
        /// <typeparam name="T">Type of passed value</typeparam>
        /// <param name="value">Value to be encoded</param>
        /// <returns>Byte array</returns>
        public byte[] EncodeValue<T>(T value) where T : /*struct,*/ /*IComparable,*/ /*IComparable<T>,*/ IConvertible /*IEquatable<T>,*/ /*IFormattable*/
        {
            if (value == null)
                throw new ArgumentNullException("Come on, we don't send nulls.");
            var enc = new List<byte>();
            switch (value.GetTypeCode())
            {
                case TypeCode.Char:
                    return EncodeValueInternal(null, BitConverter.GetBytes((char)(object)value), (byte)'C');

                case TypeCode.SByte:
                    return EncodeValueInternal(null, BitConverter.GetBytes((sbyte)(object)value), (byte)'B');
                case TypeCode.Byte:
                    return EncodeValueInternal(null, BitConverter.GetBytes((byte)(object)value), (byte)'B');

                case TypeCode.Int16:
                    return EncodeValueInternal(null, BitConverter.GetBytes((short)(object)value), (byte)'S');
                case TypeCode.UInt16:
                    return EncodeValueInternal(null, BitConverter.GetBytes((ushort)(object)value), (byte)'S');

                case TypeCode.Int32:
                    return EncodeValueInternal(null, BitConverter.GetBytes((int)(object)value), (byte)'%');
                case TypeCode.UInt32:
                    return EncodeValueInternal(null, BitConverter.GetBytes((uint)(object)value), (byte)'%');

                case TypeCode.Int64:
                    return EncodeValueInternal(null, BitConverter.GetBytes((long)(object)value), (byte)'&');
                case TypeCode.UInt64:
                    return EncodeValueInternal(null, BitConverter.GetBytes((ulong)(object)value), (byte)'&');

                case TypeCode.Single:
                    return EncodeValueInternal(null, BitConverter.GetBytes((float)(object)value), (byte)'!');
                case TypeCode.Double:
                    return EncodeValueInternal(null, BitConverter.GetBytes((double)(object)value), (byte)'#');

                case TypeCode.Decimal:
                    return EncodeValueInternal(null, BitConverterEx.GetBytes((decimal)(object)value), (byte)'@');

                case TypeCode.String:
                    if (((string)(object)value).Length > 255)
                        throw new OverflowException("String length exceedes maximum available length of 255 bytes.");
                    return EncodeValueInternal(0, Encoding.UTF8.GetBytes((string)(object)value), (byte)'$');

                default:
                    var array = value as IEnumerable<byte>;
                    if (array != null)
                    {
                        if (array.Count() > 255)
                            throw new OverflowException("Array length exceedes maximum available length of 255 bytes.");
                        return EncodeValueInternal(null, array, (byte)'*', Convert.ToByte(array.Count()));
                    }
                    throw new ArgumentException("Unexpected data type.");
            }
        }
        private byte[] EncodeValueInternal(byte? footer, IEnumerable<byte> body, params byte[] header)
        {
            return header.Concat(footer.HasValue ? body.Concat(new[] { footer.Value }) : body).ToArray();
        }

        /// <summary>
        /// Provides methods for comparing data blocks.
        /// </summary>
        public class DataBlockEqualityComparer : IEqualityComparer<IDataBlock>
        {
            public bool Equals(IDataBlock x, IDataBlock y)
            {
                return AreEqual(x, y);
            }

            public int GetHashCode(IDataBlock obj)
            {
                unchecked
                {
                    int hash = 1009;
                    hash = hash * 9176 + obj.GetHashCode();
                    hash = hash * 9176 + obj.Data.GetHashCode();
                    return hash;
                }
            }

            /// <summary>
            /// Returns <see cref="true"/> if two objects of type <see cref="IDataBlock"/> are considered equal.
            /// </summary>
            /// <param name="x">First objects of type <see cref="IDataBlock"/></param>
            /// <param name="y">Second objects of type <see cref="IDataBlock"/></param>
            /// <param name="typeCompatibility">Type comparision method</param>
            /// <returns></returns>
            public static bool AreEqual(IDataBlock x, IDataBlock y, EqualityCheck typeCompatibility = EqualityCheck.Exact)
            {
                switch (typeCompatibility)
                {
                    case EqualityCheck.Related:
                        if (!x.GetType().IsAssignableFrom(y.GetType())) return false;
                        break;
                    case EqualityCheck.RelatedReversed:
                        if (!y.GetType().IsAssignableFrom(x.GetType())) return false;
                        break;
                    case EqualityCheck.Exact:
                        if (x.GetType() != y.GetType()) return false;
                        break;
                    default:
                        throw new InvalidEnumArgumentException();
                }
                return x.Data.SequenceEqual(y.Data);
            }
            /// <summary>
            /// Type comparision method for <see cref="AreEqual"/> method.
            /// </summary>
            public enum EqualityCheck
            {
                /// <summary>
                /// Both object must be of the exact same type.
                /// </summary>
                Exact,
                /// <summary>
                /// Type of the first object must be assignable from type of the second object.
                /// </summary>
                Related,
                /// <summary>
                /// Type of the second object must be assignable from type of the first object.
                /// </summary>
                RelatedReversed
            }
        }
    }
}
