﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLink.Protocol
{
    /// <summary>
    /// Base class for communication frames.
    /// </summary>
    public abstract class Frame : DataBlock
    {
        /// <summary>
        /// Frame preamble data block.
        /// </summary>
        public abstract Preamble Preamble { get; }
        /// <summary>
        /// Start of heading marker. Default: SOH control character.
        /// </summary>
        public virtual IDataBlock StartOfHeading { get; } = new SingleByte(new ControlChar(ControlChars.SOH));
        /// <summary>
        /// Frame header data block.
        /// </summary>
        public abstract Header Header { get; }
        /// <summary>
        /// Frame header and body separator marker. Default: STX control character.
        /// </summary>
        public virtual IDataBlock StartOfText { get; } = new SingleByte(new ControlChar(ControlChars.STX));
        /// <summary>
        /// Frame body data block.
        /// </summary>
        public abstract ByteArray Message { get; }
        /// <summary>
        /// Frame body terminator marker. Default: ETX control character.
        /// </summary>
        public virtual IDataBlock EndOfText { get; } = new SingleByte(new ControlChar(ControlChars.ETX));
        /// <summary>
        /// Frame check sum data block. 
        /// By default it is a <see cref="Crc32Checksum"/> whose calculation is based upon vaules of StartOfHeading, Header, StartOfText, Message and EndOfText blocks.
        /// </summary>
        public virtual CheckSum CheckSum { get { return new Crc32Checksum(new[] { StartOfHeading, Header, StartOfText, Message, EndOfText }); } }
        /// <summary>
        /// End of frame marker. Default: EOT control character.
        /// </summary>
        public virtual IDataBlock EndOfTransmission { get; } = new SingleByte(new ControlChar(ControlChars.EOT));

        /// <summary>
        /// Returns concatenated datablocks as follows: Preamble, StartOfHeading, Header, StartOfText, Message, EndOfText, CheckSum, EndOfTransmission.
        /// </summary>
        public override IEnumerable<byte> Data
        {
            get
            {
                return Concatenate(Preamble, StartOfHeading, Header, StartOfText, Message, EndOfText, CheckSum, EndOfTransmission);
            }
        }
        private IEnumerable<byte> Concatenate(params IDataBlock[] sequences)
        {
            return sequences.SelectMany(s => s.Data);
        }

        /// <summary>
        /// If set to true informs the controller to not to put this frame into queue when the queue is not empty.
        /// </summary>
        public bool Overridable { get; set; }

        /// <summary>
        /// Sets the discarded state for this frame so it will not be sent to the remote device when dequeued.
        /// </summary>
        public void Discard()
        {
            IsDiscarded = true;
        }
        /// <summary>
        /// Indicates that this frame has been discarded and is not eligible to send to the remote device.
        /// </summary>
        public bool IsDiscarded { get; private set; }

        public override bool Equals(IDataBlock other)
        {
            return DataBlockEqualityComparer.AreEqual(this, other, DataBlockEqualityComparer.EqualityCheck.Related);
        }

        public Frame()
        {
        }


        public enum ResponseKind
        {
            Affirmative,
            Negative
        }
    }



    #region Data blocks
    /// <summary>
    /// Represents one byte data block.
    /// </summary>
    public class SingleByte : DataBlock
    {
        public override IEnumerable<byte> Data { get; }
        public SingleByte(byte data)
        {
            Data = new[] { data };
        }
    }
    /// <summary>
    /// Represents multiple byte data block.
    /// </summary>
    public class ByteArray : DataBlock
    {
        public override IEnumerable<byte> Data { get; }
        public ByteArray(byte[] data)
        {
            Data = data;
        }
    }

    /// <summary>
    /// Represents frame preamble block. By default it's one character 170 (10101010).
    /// </summary>
    public abstract class Preamble : DataBlock
    {
        protected virtual byte Character { get; } = 170;
        protected virtual int Length { get; } = 1;
        private Lazy<IEnumerable<byte>> _data;
        public Preamble()
        {
            _data = new Lazy<IEnumerable<byte>>(() => Enumerable.Repeat(Character, Length));
        }
        public override IEnumerable<byte> Data
        {
            get { return _data.Value; }
        }
    }
    /// <summary>
    /// Four character default preamble.
    /// </summary>
    public class LongPreamble : Preamble
    {
        protected override int Length { get { return 4; } }
    }
    /// <summary>
    /// Two character default preamble.
    /// </summary>
    public class ShortPreamble : Preamble
    {
        protected override int Length { get { return 2; } }
    }

    /// <summary>
    /// Represents frame header block. Supports lazy access to <see cref="Data"/> property.
    /// </summary>
    public class LazyHeader : Header
    {
        /// <summary>
        /// Main header identifier.
        /// </summary>
        public byte PrimaryCharacter { get; }
        /// <summary>
        /// Additional header identifier.
        /// </summary>
        public byte? ComplementaryCharacter { get; }

        /// <summary>
        /// Initialize new <see cref="Header"/> instance.
        /// </summary>
        /// <param name="primary">Main header identifier.</param>
        /// <param name="complementary">Additional header identifier.</param>
        public LazyHeader(byte primary, byte? complementary = null) : base()
        {
            PrimaryCharacter = primary;
            ComplementaryCharacter = complementary;
        }

        public override IEnumerable<byte> Data
        {
            get
            {
                var data = new List<byte>() { PrimaryCharacter };
                if (ComplementaryCharacter.HasValue) data.Add(ComplementaryCharacter.Value);
                return data;
            }
        }
    }
    /// <summary>
    /// Represents frame header block.
    /// </summary>
    public class Header : DataBlock
    {
        public override IEnumerable<byte> Data { get; }

        /// <summary>
        /// Initialize new <see cref="Header"/> instance.
        /// </summary>
        /// <param name="primary">Main header identifier.</param>
        /// <param name="complementary">Additional header identifier.</param>
        public Header(byte primary, byte? complementary = null)
        {
            var data = new List<byte>() { primary };
            if (complementary.HasValue) data.Add(complementary.Value);
            Data = data;
        }

        /// <summary>
        /// Constructor for <see cref="LazyHeader"/> support.
        /// </summary>
        protected Header() { }
    }

    /// <summary>
    /// Represents frame check sum block.
    /// </summary>
    public abstract class CheckSum : DataBlock
    {
        public override IEnumerable<byte> Data { get; }

        public abstract IDataBlock Identifier { get; }

        /// <summary>
        /// Initialize new <see cref="CheckSum"/> instance with fixed value.
        /// </summary>
        /// <param name="value">CheckSum value</param>
        public CheckSum(IEnumerable<byte> value)
        {
            Data = value;
        }

        /// <summary>
        /// Initialize new <see cref="CheckSum"/> instance with generated value based on passed data blocks.
        /// </summary>
        /// <param name="blocks">Collection of data blocks</param>
        public CheckSum(IEnumerable<IDataBlock> blocks)
        {
            Data = GenerateCheckSum(blocks);
        }

        /// <summary>
        /// Used to generate checksum value.
        /// </summary>
        /// <returns>IEnumerable of Byte representing CheckSum value.</returns>
        protected abstract IEnumerable<byte> GenerateCheckSum(IEnumerable<IDataBlock> blocks);

        public bool Verify(IEnumerable<byte> data)
        {
            return data.SequenceEqual(Data);
        }
    }
    /// <summary>
    /// Provides CRC32 check sum.
    /// </summary>
    public class Crc32Checksum : CheckSum
    {
        public override IDataBlock Identifier { get { return new SingleByte((byte)'%'); } }

        /// <summary>
        /// Initialize new <see cref="Crc32Checksum"/> instance with fixed value.
        /// </summary>
        /// <param name="value">CheckSum value</param>
        public Crc32Checksum(IEnumerable<byte> value) : base(value) { }

        /// <summary>
        /// Initialize new <see cref="Crc32Checksum"/> instance with generated value based on passed data blocks.
        /// </summary>
        /// <param name="blocks">Collection of data blocks</param>
        public Crc32Checksum(IEnumerable<IDataBlock> blocks) : base(blocks) { }

        protected override IEnumerable<byte> GenerateCheckSum(IEnumerable<IDataBlock> blocks)
        {
            return EncodeValue(CyclicRedundancyCheck.GetCRC32(blocks.SelectMany(b => b.Data)));
        }
    }
    #endregion


    /// <summary>
    /// Contains extension method to send frame through <see cref="SerialPort"/>.
    /// </summary>
    public static class SerialPortExtension
    {
        /// <summary>
        /// Send frame data through this <see cref="SerialPort"/>.
        /// </summary>
        /// <param name="port">Serial port</param>
        /// <param name="frame">Frame whose data are to be sent.</param>
        public static void SendFrame(this SerialPort port, Frame frame)
        {
            var buffer = frame.Data.ToArray();
            port.Write(buffer, 0, buffer.Length);
        }
    }
}
