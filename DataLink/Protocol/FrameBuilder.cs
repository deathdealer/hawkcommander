﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLink.Protocol
{
    /// <summary>
    /// <para>Provides functionality of decoding data into <see cref="Frame"/>.</para>
    /// <para>This version supports only frames with single-byte markers.</para>
    /// </summary>
    public class FrameBuilder
    {
        private Frame _dummy;
        private FrameBuilderState _state = FrameBuilderState.Preamble;
        private bool _escape = false;
        protected Frames.ReceiveFrame _newFrame = null;
        //private Queue<byte> _buffer = new Queue<byte>();

        public class FrameReadyEventArgs : EventArgs
        {
            Frame Frame { get; }
            public FrameReadyEventArgs(Frame frame)
            {
                Frame = frame;
            }
        }
        public event EventHandler<FrameReadyEventArgs> FrameReady;
        protected void OnFrameReady()
        {
            FrameReady?.Invoke(this, new FrameReadyEventArgs(_newFrame));
            _newFrame = null;
        }

        public virtual void AddData(IEnumerable<byte> data)
        {
            foreach (var dataByte in data)
            {
                switch (_state)
                {
                    case FrameBuilderState.Preamble:
                        if (dataByte == _dummy.StartOfHeading.Data.First())
                            _state = FrameBuilderState.Header;
                        _newFrame = new Frames.ReceiveFrame();
                        break;
                    case FrameBuilderState.Header:
                        if (dataByte == _dummy.StartOfText.Data.First())
                            _state = FrameBuilderState.Header;
                        _newFrame.AppendHeader(dataByte);
                        break;
                    case FrameBuilderState.Message:
                        if (dataByte == _dummy.EndOfText.Data.First())
                            _state = FrameBuilderState.Header;
                        _newFrame.AppendMessage(dataByte);
                        break;
                    case FrameBuilderState.Checksum:
                        if (dataByte == _dummy.EndOfTransmission.Data.First())
                            OnFrameReady();
                        _newFrame.AppendMessage(dataByte);
                        break;
                }
            }
        }

        public FrameBuilder(Frame template)
        {
            _dummy = template;
        }
    }

    public enum FrameBuilderState
    {
        Preamble,
        Header,
        Message,
        Checksum
    }

}
