﻿using DataLink.Protocol;
using DataLink.Protocol.Frames;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace DataLink
{
    /// <summary>
    /// Provides functionality of connecting and sending/receiving messages to/from remote device.
    /// </summary>
    public class Transport
    {
        #region Fields
        private System.Threading.Timer _transmitTimer;

        private ConcurrentQueue<Frame> _frameQueue = new ConcurrentQueue<Frame>();

        private ConcurrentQueue<byte> _inputBuffer = new ConcurrentQueue<byte>();

        private FrameBuilder _frameBuilder = new FrameBuilder(new ReceiveFrame());

        private ConcurrentBag<FrameId> _expectedIds = new ConcurrentBag<FrameId>();
        #endregion

        #region Properties
        /// <summary>
        /// <see cref="SerialPort"/> object used to communicate with remote device.
        /// </summary>
        public SerialPort Port { get; } = new SerialPort()
        {
            PortName = "COM1",
            BaudRate = 57600
        };


        public bool PortOpen { get { return Port.IsOpen; } }


        [DefaultValue("COM3")]
        public string PortName
        {
            get { return Port.PortName; }
            set { Port.PortName = value; }
        }

        [DefaultValue(57600)]
        public int BaudRate
        {
            get { return Port.BaudRate; }
            set { Port.BaudRate = value; }
        }

        public int TransmitInterval { get; set; } = 1;

        public int ResponseTimeout { get; set; } = 666;
        #endregion

        #region Events
        public class ErrorOccuredEventArgs : EventArgs
        {
            public Exception Exception { get; }
            public ErrorOccuredEventArgs(Exception ex)
            {
                Exception = ex;
            }
        }
        /// <summary>
        /// Event raised when an error occurs during communication process.
        /// </summary>
        public event EventHandler<ErrorOccuredEventArgs> ErrorOccured;
        /// <summary>
        /// Raises <see cref="ErrorOccured"/> event.
        /// </summary>
        /// <param name="exception">A thrown <see cref="Exception"/>.</param>
        protected void OnErrorOccured(Exception exception)
        {
            ErrorOccured?.Invoke(this, new ErrorOccuredEventArgs(exception));
        }

        public enum ReceivedResponseType
        {
            Affirmative,
            Negative
        }
        public class ResponseReceivedEventArgs : EventArgs
        {
            public ReceivedResponseType ResponseType { get; }
            public int Id { get; }
            public object Data { get; }
            public ResponseReceivedEventArgs(ReceivedResponseType responseType, int id, object data)
            {
                ResponseType = responseType;
                Id = id;
                Data = data;
            }
        }
        public event EventHandler<ResponseReceivedEventArgs> ResponseReceived;
        protected void OnResponseReceived(ReceivedResponseType responseType, int id, object data)
        {
            ResponseReceived?.Invoke(this, new ResponseReceivedEventArgs(responseType, id, data));
        }


        //public event EventHandler PortReady;

        //public event EventHandler<EventArgs> AffirmativeResponse;

        //public event EventHandler NegativeResponse;
        #endregion

        #region Constructors
        public Transport()
        {
            Port.DataReceived += Port_DataReceived;
            //Port.ErrorReceived += Port_ErrorReceived;
            _transmitTimer = new System.Threading.Timer(new System.Threading.TimerCallback(Timer_Tick));
        }
        #endregion

        #region Methods
        /// <summary>
        /// Establishes connection with remote device using current <see cref="SerialPort"/> settings.
        /// </summary>
        //[DebuggerStepThrough]
        public async Task Connect()
        {
            await Task.Run(() =>
            {
                ClearFrameQueue();
                try { Port.Open(); }
                catch /*(Exception ex)*/ { throw /*ex*/; }
                _transmitTimer.Change(TransmitInterval, System.Threading.Timeout.Infinite);
            });
        }

        /// <summary>
        /// Closes current connection.
        /// </summary>
        public async Task Disconnect()
        {
            await Task.Run(() =>
            {
                _transmitTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
                try { Port.Close(); } catch { }
                ClearFrameQueue();
            });
        }

        /// <summary>
        /// Adds new <see cref="TransmitFrame"/> with specified header and message to the frame queue.
        /// </summary>
        /// <param name="header">Frame <see cref="Header"/></param>
        /// <param name="message"><see cref="ByteArray"/> with frame message</param>
        public void EnqueueFrame(Header header, ByteArray message, int id)
        {
            EnqueueFrame(new TransmitFrame(header, message), id);
        }

        public void EnqueueFrame(Frame frame, int id)
        {
            _frameQueue.Enqueue(frame);
            _expectedIds.Add(new FrameId(id, ResponseTimeout));
        }
        #endregion

        #region Code
        private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

        }

        private void Timer_Tick(object state)
        {
            Frame frame;
            if (_frameQueue.TryDequeue(out frame))
                if (!frame.IsDiscarded)
                    if (Port.IsOpen)
                        Port.SendFrame(frame);
            _transmitTimer.Change(TransmitInterval, System.Threading.Timeout.Infinite);
        }

        private void ClearFrameQueue()
        {
            Frame frame;
            while (_frameQueue.TryDequeue(out frame)) ;
        }
        #endregion

        #region Classes
        private class FrameId
        {
            private System.Threading.Timer _watchdog;

            public int Id { get; }
            public int Timeout { get; set; }
            public Guid Guid { get; } = Guid.NewGuid();

            public event EventHandler Expired;

            public FrameId(int id, int timeout)
            {
                Id = id;
                Timeout = timeout;
                _watchdog = new System.Threading.Timer(new System.Threading.TimerCallback(WatchDogBark), null, timeout, System.Threading.Timeout.Infinite);
            }
            public void Reset(int timeout = -1)
            {
                if (timeout > 0) Timeout = timeout;
                _watchdog.Change(Timeout, System.Threading.Timeout.Infinite);
            }

            private void WatchDogBark(object state)
            {
                Expired?.Invoke(this, EventArgs.Empty);
            }
        }
        #endregion
    }
}
