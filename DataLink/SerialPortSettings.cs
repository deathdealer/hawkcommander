﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLink
{
    [Obsolete("Approach abandoned")]
    [Serializable]
    public class SerialPortSettings
    {
        private SerialPort Port { get; set; }

    }
}
