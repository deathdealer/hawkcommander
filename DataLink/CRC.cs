﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DataLink
{
    /// <summary>
    /// Provides static methods to compute various CRC values for given data.
    /// </summary>
    /// <remarks>
    /// Based upon:
    ///     WebSite: http://cool-emerald.com (dead link in 16 October 2016)
    ///     License: Creative Commons Attribution-ShareAlike 3.0
    ///              http://creativecommons.org/licenses/by-sa/3.0/
    ///     Programmer: Yan Naing Aye
    ///     Date: 04 June 2009
    ///     Updated: 25 September 2009
    /// </remarks>
    public class CyclicRedundancyCheck
    {
        /// <summary>
        /// <para>CRC Order: 16</para>
        /// <para>CCITT(recommendation): F(x) = x16 + x12 + x5 + 1</para>
        /// <para>CRC Poly: 0x1021</para>
        /// <para>Operational initial value: 0xFFFF</para>
        /// <para>Final XOR value: 0</para>
        /// </summary>
        /// <param name="data">Data byte array</param>
        /// <param name="seed">Initial CRC value</param>
        /// <returns><see cref="ushort"/> CRC value</returns>
        public static ushort GetCRC16CCITT(IEnumerable<byte> data, ushort seed = 0xFFFF)
        {
            var table = new ushort[256];
            #region Init
            var crc = default(ushort);
            for (ushort i = 0; i <= 255; i++)
            {
                crc = (ushort)(i << 8);
                for (int j = 0; j <= 7; j++)
                {
                    if ((crc & 0x8000) == 0x8000)
                    {
                        crc = (ushort)((crc << 1) ^ 0x1021);
                    }
                    else
                    {
                        crc = (ushort)(crc << 1);
                    }
                }
                table[i] = crc;
            }
            #endregion

            var CRC = seed;
            foreach (byte b in data)
            {
                CRC = (ushort)((CRC << 8) ^ table[(CRC >> 8) ^ Convert.ToUInt16(b)]);
            }
            return CRC;
        }

        /// <summary>
        /// <para>CRC Order: 16</para>
        /// <para>CCITT(recommendation): F(x) = x16 + x12 + x2 + 1</para>
        /// <para>CRC Poly: 0x8005 &lt;=&gt; 0xA001</para>
        /// <para>Operational initial value: 0x0000</para>
        /// <para>Final XOR value: 0</para>
        /// </summary>
        /// <param name="data">Data byte array</param>
        /// <param name="seed">Initial CRC value</param>
        /// <returns><see cref="ushort"/> CRC value</returns>
        public static ushort GetCRC16(IEnumerable<byte> data, ushort seed = 0x0)
        {
            var table = new ushort[256];
            #region Init
            var crc = default(ushort);
            for (ushort i = 0; i <= 255; i++)
            {
                crc = i;
                for (int j = 0; j <= 7; j++)
                {
                    if ((crc & 0x1) == 0x1)
                    {
                        crc = (ushort)((crc >> 1) ^ 0xA001);
                    }
                    else
                    {
                        crc = (ushort)(crc >> 1);
                    }
                }
                table[i] = crc;
            }
            #endregion

            var CRC = seed;
            foreach (byte b in data)
            {
                CRC = (ushort)((CRC >> 8) ^ table[(CRC >> 0xFF) ^ Convert.ToUInt16(b)]);
            }
            return CRC;
        }

        /// <summary>
        /// <para>CRC Order: 32</para>
        /// <para>CRC Poly: 0x04C11DB7 &lt;=&gt; 0xEDB88320</para>
        /// <para>Operational initial value:  0xFFFFFFFF</para>
        /// <para>Final XOR value: 0xFFFFFFFF</para>
        /// </summary>
        /// <param name="data">Data byte array</param>
        /// <param name="seed">Initial CRC value</param>
        /// <returns><see cref="uint"/> CRC value</returns>
        public static uint GetCRC32(IEnumerable<byte> data, uint seed = 0xFFFFFFFF)
        {
            var table = new uint[256];
            #region Init
            uint crc = default(uint);
            for (uint i = 0; i <= 255; i++)
            {
                crc = i;
                for (int j = 0; j <= 7; j++)
                {
                    if ((crc & 0x1) == 0x1)
                    {
                        crc = (crc >> 1) ^ 0xEDB88320;
                    }
                    else
                    {
                        crc = crc >> 1;
                    }
                }
                table[i] = crc;
            }
            #endregion

            var CRC = seed;
            foreach (byte b in data)
            {
                CRC = (CRC >> 8) ^ table[(CRC & 0xff) ^ Convert.ToUInt32(b)];
            }
            CRC = ~CRC;
            return CRC;
        }

        /// <summary>
        /// Checksum: XOR of all bytes
        /// </summary>
        /// <param name="data">Data byte array</param>
        /// <returns><see cref="byte"/> CRC value</returns>
        public static byte GetCRC8(IEnumerable<byte> data)
        {
            byte CS = 0x0;
            foreach (byte b in data)
            {
                CS = (byte)(CS ^ b);
            }
            return CS;
        }
    }
}
