﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transceiver
{
    /// <summary>
    /// Contains available controller states
    /// </summary>
    public enum ControllerState
    {
        /// <summary>
        /// Indicates that the port is closed.
        /// </summary>
        PortClosed,

        /// <summary>
        /// Indicates the state when an attempt to open the port is in progress.
        /// </summary>
        PortOpening,

        /// <summary>
        /// Indicates that the port is open but the remote device is not connected.
        /// </summary>
        PortOpen,

        /// <summary>
        /// Indicates that the connection with the remote device has been established.
        /// </summary>
        ConnectionEstablished,

        /// <summary>
        /// Indicates the state when the port is being closed and the frame queue is being cleaned up.
        /// </summary>
        Disconnecting
    }
}
