﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transceiver
{
    public abstract class RemoteValueSetter<T>
    {
        public DataLink.Transport Controller { get; set; }

        public virtual void SetValue(T value, int id)
        {
            Controller?.EnqueueFrame(GetFrameHeader(value), GetFrameMessage(value), id);
        }

        protected abstract DataLink.Protocol.Header GetFrameHeader(T value);
        protected abstract DataLink.Protocol.ByteArray GetFrameMessage(T value);
    }
}
