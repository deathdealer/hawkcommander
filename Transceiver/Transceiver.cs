﻿using DataLink.Protocol.Frames;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Transceiver
{
    public abstract class Transceiver : INotifyPropertyChanged
    {
        #region Fields
        protected DataLink.Transport _DataLinkController = null;
        #endregion

        #region Properties
        [Obsolete("Don't use this field. Please use corresponding property.")]
        private ControllerState _state;
        /// <summary>
        /// Gets the state of the connection controller. When set internally, raises the <see cref="StateChanged"/> event.
        /// </summary>
        public ControllerState State
        {
#pragma warning disable CS0618 //Disable Obsolete warning.
            get { return _state; }
            private set
            {
                _state = value;
                StateChanged?.Invoke(this, EventArgs.Empty);
            }
#pragma warning restore CS0618 //Restore Obsolete warning.
        }

        protected abstract IntroduceFrameBase IntroduceFrame { get; }
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = default(string))
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event EventHandler StateChanged;
        #endregion

        #region Constructor
        public Transceiver()
        {
            _DataLinkController = new DataLink.Transport();
        }
        #endregion

        #region Methods
        public async Task Connect()
        {
            if (State == ControllerState.PortClosed)
            {
                State = ControllerState.PortOpening;
                try
                {
                    await _DataLinkController.Connect();
                    State = ControllerState.PortOpen;
                    //_DataLinkController.EnqueueFrame(
                }
                catch
                {
                    State = ControllerState.PortClosed;
                    throw;
                }
            }
            else
            {
                throw new InvalidOperationException($"This method is valid only in {ControllerState.PortClosed.ToString()} state.");
            }
        }

        public async Task Disconnect()
        {
            if (State != ControllerState.PortClosed)
            {
                State = ControllerState.Disconnecting;
                await _DataLinkController.Disconnect();
                State = ControllerState.PortClosed;
            }
            else
            {
                throw new InvalidOperationException($"This method is invalid in {ControllerState.PortClosed.ToString()} state.");
            }
        }

        public TransmitterRequest EstablishLink()
        {
            //_DataLinkController.Connect
            return new TransmitterRequest();
        }

        public void UpdateGui()
        {

        }

        /// <summary>
        /// Sends specified value to the remote device.
        /// </summary>
        /// <typeparam name="TSetter">Type derived from <see cref="RemoteValueSetter"/> containing definition of the command to set desired value</typeparam>
        /// <typeparam name="TValue">Type of the value being set</typeparam>
        /// <param name="value">Object representing value to be set</param>
        /// <returns></returns>
        public TransmitterRequest SetRemoteValue<TSetter, TValue>(TValue value) where TSetter : RemoteValueSetter<TValue>, new()
        {
            new TSetter() { Controller = _DataLinkController }.SetValue(value, GetFrameId());
            return new TransmitterRequest();
        }
        #endregion

        #region Code
        private int GetFrameId()
        {
            return Guid.NewGuid().GetHashCode();
        }
        #endregion
    }

}
