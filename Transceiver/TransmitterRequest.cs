﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transceiver
{
    public class TransmitterRequest
    {
        public int Timeout { get; set; } = 1000;
        public event EventHandler ResponseReceived;
    }

    public class ReceiverResponseEventArgs : EventArgs
    {
        public ReceiverResponseType ResponseType { get; }

        public ReceiverResponseEventArgs(ReceiverResponseType responseType)
        {
            ResponseType = responseType;
        }
    }

    public enum ReceiverResponseType
    {
        Success,
        Failure,
        Dropped
    }
}
