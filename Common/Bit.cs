﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    /// <summary>
    /// Provides functionality of normalized conversion from boolean to byte.
    /// </summary>
    public class Bit
    {
        public virtual byte HighState { get { return (byte)'T'; } }
        public virtual byte LowState { get { return (byte)'f'; } }

        bool State { get; }
        public Bit(bool state)
        {
            State = state;
        }

        public static implicit operator byte(Bit bit)
        {
            return bit.State ? bit.HighState : bit.LowState;
        }
    }
}
