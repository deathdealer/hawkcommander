﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HawkCommander
{
    internal class ColoredBackgroundImage
    {
        private Image _template;
        private Dictionary<Color, Image> _images = new Dictionary<Color, Image>();
         
        public ColoredBackgroundImage(Image template)
        {
            _template = template;
        }

        public Image GetImage(Color backgroundColor)
        {
            if (!_images.ContainsKey(backgroundColor))
            {
                _images.Add(backgroundColor, PaintImageBackground(_template, backgroundColor));
            }
            return _images[backgroundColor];
        }

        private static Image PaintImageBackground(Image source, Color color)
        {
            var bmp = new Bitmap(source.Width, source.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.FillRectangle(new SolidBrush(color), new Rectangle(new Point(), bmp.Size));
                g.DrawImage(source, new Point());
            }
            return bmp;
        }

    }
}
