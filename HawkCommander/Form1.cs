﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HawkCommander
{
    public partial class Form1 : Form
    {
        private Transceiver.Transceiver _transceiver;
        private System.Threading.Timer _newLineTimer;
        private ColoredBackgroundImage _connectionButtonImage;

        public Form1()
        {
            InitializeComponent();

            _connectionButtonImage = new HawkCommander.ColoredBackgroundImage(toolStripButton1.Image);
            toolStripButton1.DropDownItems.Add(new ToolStripControlHost(new PropertyGrid()
            {
                SelectedObject = serialPort1,
                Dock = DockStyle.Fill
            })
            {
                Size = new Size(400, 300),
                AutoSize = false
            });

            _transceiver = new Transceiver.Bsc_4B00.Bsc_4B09();
            _transceiver.StateChanged += Transceiver_StateChanged;
            _newLineTimer = new System.Threading.Timer(new System.Threading.TimerCallback(NewLineTimerTick));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
        private void Form1_Shown(object sender, EventArgs e)
        {
            Transceiver_StateChanged(sender, e);
        }

        private async void toolStripButton1_ButtonClick(object sender, EventArgs e)
        {
            if (_transceiver.State == Transceiver.ControllerState.PortOpen)
            {
                await _transceiver.Disconnect();
            }
            else if (_transceiver.State == Transceiver.ControllerState.PortClosed)
            {
                try
                {
                    await _transceiver.Connect();
                }
                catch (Exception ex)
                {
                    toolTip1.ToolTipTitle = ex.Message;
                    toolTip1.ToolTipIcon = ToolTipIcon.Error;
                    toolTip1.Show("Error connecting transceiver", toolStrip1, 0, toolStrip1.Height, 8192);
                }
            }
        }

        private void Transceiver_StateChanged(object sender, EventArgs e)
        {
            switch (_transceiver.State)
            {
                case Transceiver.ControllerState.PortClosed:
                    toolStripButton1.Image = _connectionButtonImage.GetImage(Color.DarkRed);
                    toolStripButton1.Text = "Połącz";
                    break;
                case Transceiver.ControllerState.PortOpening:
                    toolStripButton1.Image = _connectionButtonImage.GetImage(Color.Gold);
                    break;
                case Transceiver.ControllerState.PortOpen:
                    toolStripButton1.Image = _connectionButtonImage.GetImage(Color.CadetBlue);
                    toolStripButton1.Text = "Rozłącz";
                    break;
                case Transceiver.ControllerState.ConnectionEstablished:
                    toolStripButton1.Image = _connectionButtonImage.GetImage(Color.DarkGreen);
                    toolStripButton1.Text = "Rozłącz";
                    break;
                case Transceiver.ControllerState.Disconnecting:
                    toolStripButton1.Image = _connectionButtonImage.GetImage(Color.Red);
                    break;
                default:
                    toolStripButton1.Image = _connectionButtonImage.GetImage(Color.Gray);
                    break;
            }
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (toolStripButton2.Checked)
            {
                var buffer = new byte[serialPort1.BytesToRead];
                serialPort1.Read(buffer, 0, buffer.Length);
                AppendWindowText(buffer);
            }
            else
            {
                AppendWindowText(serialPort1.ReadExisting());
            }
        }
        private void AppendWindowText(string text, bool noDelayedNewLine = false)
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegate { AppendWindowText(text, noDelayedNewLine); }));
            }
            else
            {
                richTextBox1.AppendText(text);
                richTextBox1.ScrollToCaret();
                if (!noDelayedNewLine) _newLineTimer.Change(1138, System.Threading.Timeout.Infinite);
            }
        }
        private void AppendWindowText(byte[] bytes)
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegate { AppendWindowText(bytes); }));
            }
            else
            {
                richTextBox1.AppendText(string.Concat(bytes.Select(b => string.Format("0x{0:X2} ", b))));
                _newLineTimer.Change(1138, System.Threading.Timeout.Infinite);
            }
        }
        private void NewLineTimerTick(object state)
        {
            AppendWindowText(Environment.NewLine, true);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
        }
        private void toolStripButton3_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = toolStripButton3.Checked;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            var result = _transceiver.SetRemoteValue<Transceiver.Bsc_4B00.Led1Setter, bool>(true);
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

    }
}
