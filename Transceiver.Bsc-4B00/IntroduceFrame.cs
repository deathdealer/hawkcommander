﻿using DataLink;
using DataLink.Protocol.Frames;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transceiver.Bsc_4B00
{
    public class IntroduceFrame : IntroduceFrameBase
    {
        protected override Int64 ProtocolId
        {
            get
            {
                UInt32 msw = 0x07804A00; //0780(1920dec): Commander spritenum, 4: years since 2012, A: month, 00: CRC = CD
                UInt32 lsw = 0x9C087300; //9C(156dec): ASCII £, 0873(2163dec): JH-2163, 00: CRC = E7
                msw += CyclicRedundancyCheck.GetCRC8(BitConverter.GetBytes(msw));
                lsw += CyclicRedundancyCheck.GetCRC8(BitConverter.GetBytes(lsw));
                return (long)((((ulong)msw) << 32) | lsw);
            }
        }
    }
}
