﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLink.Protocol;
using Common;

namespace Transceiver.Bsc_4B00
{
    public abstract class LedSetter : RemoteValueSetter<bool>
    {
        protected override Header GetFrameHeader(bool value)
        {
            return new Header(new ControlChar(ControlChars.DC4));
        }
    }
    public class Led1Setter : LedSetter
    {
        protected override ByteArray GetFrameMessage(bool value)
        {
            return new ByteArray(new[] { (byte)0x11, new Bit(value) }); //0x11: P1.1
        }
    }
}
