﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLink.Protocol.Frames;

namespace Transceiver.Bsc_4B00
{
    public class Bsc_4B09 : Transceiver
    {
        #region Properties
        protected override IntroduceFrameBase IntroduceFrame
        {
            get
            {
                return new IntroduceFrame();
            }
        }
        #endregion

        #region Constroctor
        public Bsc_4B09() : base()
        {

        }


        #endregion
    }
}
