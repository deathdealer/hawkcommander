﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace UnitTestProject1
{
    [TestClass]
    public class CRC_Calculator
    {
        [TestMethod]
        public void GetCRC8(/*IEnumerable<byte> data*/)
        {
            var data = new byte[] { (byte)'B', (byte)'s', (byte)'c', (byte)'-', (byte)'4', (byte)'B', (byte)'0', (byte)'0' };
            Console.WriteLine(DataLink.CyclicRedundancyCheck.GetCRC8(data));
        }
    }
}
